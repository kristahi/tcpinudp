/*-
 * Copyright (c) 2015, 2016 Kristian A. Hiorth
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/cdefs.h>
__FBSDID("$FreeBSD$");

#include <sys/param.h>
#include <sys/kernel.h>
#include <sys/malloc.h>
#include <sys/module.h>
#include <sys/socket.h>
#include <sys/socketvar.h>
#include <sys/sysctl.h>
#include <sys/systm.h>
#include <sys/ktr.h>

#include <net/vnet.h>

#include <netinet/cc.h>
#include <netinet/tcp_seq.h>
#include <netinet/tcp_var.h>
#include <netinet/tcp_hijack.h>

#include <netinet/cc/cc_fse.h>

static VNET_DEFINE(struct fse_state, cc_fse_state);
#define V_cc_fse_state VNET(cc_fse_state)

static VNET_DEFINE(int, cc_fse_fgbuckets) = 2048;
#define V_cc_fse_fgbuckets VNET(cc_fse_fgbuckets)

static VNET_DEFINE(struct callout, cc_fse_reaper_callout);
#define V_cc_fse_reaper_callout VNET(cc_fse_reaper_callout)

static VNET_DEFINE(int, cc_fse_reaper_ival) = 90;
#define V_cc_fse_reaper_ival VNET(cc_fse_reaper_ival)

static VNET_DEFINE(int, cc_fse_reaper_limit) = 180;
#define V_cc_fse_reaper_limit VNET(cc_fse_reaper_limit)

VNET_DEFINE(uint8_t, cc_fse_default_prio) = FSE_DEFAULT_PRIORITY;

static MALLOC_DEFINE(M_FSE, "FSE data",
		     "FSE congestion control data");

static struct fse_fg* cc_fse_lookup_addrs(struct in_addr faddr, struct in_addr laddr);
static void cc_fse_reaper(void *arg);

static void
cc_fse_init(void)
{
	CTR0(KTR_FSE, "Flow State Exchange initializing...\n");

	rw_init(&V_cc_fse_state.fse_statelock, "FSE global state lock");
	V_cc_fse_state.fse_fgtable = hashinit(V_cc_fse_fgbuckets, M_FSE,
					      &V_cc_fse_state.fse_fgtable_mask);

	callout_init(&V_cc_fse_reaper_callout, CALLOUT_MPSAFE);
	callout_reset(&V_cc_fse_reaper_callout, V_cc_fse_reaper_ival * hz,
		      cc_fse_reaper, curvnet);
}

static struct fse_fg*
cc_fse_lookup_addrs(struct in_addr faddr, struct in_addr laddr)
{
	struct fse_fg_head *head;
	struct fse_fg *fg;
	struct fse_flow *first;
	FSE_STATE_LOCK_ASSERT(V_cc_fse_state);

	head = &V_cc_fse_state.fse_fgtable[ \
		FSE_FG_ADDRHASH(faddr.s_addr, laddr.s_addr, V_cc_fse_state.fse_fgtable_mask)];
	LIST_FOREACH(fg, head, fg_fgte) {
		FSE_FG_MBR_RLOCK(fg);
		first = LIST_FIRST(&fg->fg_members);
		/*
		 * FIXME reading from INPCB without a lock. Can I get
		 * away with it? Should I just copy the addrs instead?
		 */
		if (first != NULL
		    && first->f_inp->inp_faddr.s_addr == faddr.s_addr
		    && first->f_inp->inp_laddr.s_addr == laddr.s_addr) {
			FSE_FG_MBR_UNLOCK(fg);
			return fg;
		}
		FSE_FG_MBR_UNLOCK(fg);
	}

	return NULL;
}

/* Write cwnd, ssthresh back to TCB, clamping values */
static inline void
cc_fse_writeback(struct fse_flow *f)
{
	struct tcpcb *tcb;

	tcb = intotcpcb(f->f_inp);

	CTR5(KTR_FSE, "%s: writing f_cwnd=%u, f_sst=%u. s_cwnd=%u, s_sst=%u.",
	     __func__, f->f_cwnd, f->f_ssthresh, f->f_fg->fg_s_cwnd,
	     f->f_fg->fg_s_ssthresh);

	tcb->snd_cwnd = max(f->f_cwnd, tcb->t_maxseg);
	tcb->snd_ssthresh = max(f->f_ssthresh, tcb->t_maxseg * 2);
}

static void
cc_fse_couple(struct fse_flow *f)
{
	struct fse_fg *fg;
	struct tcpcb *tcb;

	tcb = intotcpcb(f->f_inp);

	fg = f->f_fg;
	FSE_FG_MBR_WLOCK_ASSERT(fg);

	fg->fg_s_p += f->f_p;
	fg->fg_num_coupled += 1;

	if (fg->fg_num_coupled == 1) {
		/*
		 * FG inherits cwnd/ssthresh of first flow coupled to
		 * it.
		 * If an FG went stale, but is revived, we reuse the
		 * old params. This is temporal sharing.
		 */
		if (fg->fg_s_cwnd == 0) {
			fg->fg_s_cwnd = tcb->snd_cwnd;
			fg->fg_s_ssthresh = 0;
		}
		fg->fg_coco = f;
	}

	f->f_cwnd = f->f_p * fg->fg_s_cwnd / fg->fg_s_p;
	f->f_ssthresh = tcb->snd_ssthresh;

	if (fg->fg_s_ssthresh > 0)
		f->f_ssthresh = f->f_p * fg->fg_s_ssthresh / fg->fg_s_p;

	cc_fse_writeback(f);
	f->f_state = FSE_FLOW_COUPLED;
	CTR6(KTR_FSE, "%s(%p): flow %u coupled to fg %p. CoCo=%u (%p)",
	     __func__, f, f->f_fi, fg, fg->fg_coco->f_fi, fg->fg_coco);
}

static void
cc_fse_decouple(struct fse_flow *f)
{
	struct fse_fg *fg;
	struct fse_flow *fg_f, *candidate;
	struct tcpcb *tp;

	if (!(f->f_state & FSE_FLOW_COUPLED))
		return;

	fg = f->f_fg;
	FSE_FG_MBR_WLOCK_ASSERT(fg);

	fg->fg_s_p -= f->f_p;
	fg->fg_num_coupled -= 1;

	if (fg->fg_coco == f) {
		/*
		 * Select new CoCo:
		 * Pick the first flow in CA we find. If there are
		 * none, we prefer a flow in FR over one in SS.
		 */
		candidate = NULL;
		LIST_FOREACH(fg_f, &fg->fg_members, f_fge) {
			if (fg_f->f_state & FSE_FLOW_UNCOUPLED)
				continue;

			tp = intotcpcb(fg_f->f_inp);
			if (candidate != NULL && (fg_f->f_state & FSE_FLOW_WANTED_SS)) {
				/* Skip SS flows except if we have
				 * nothing better */
				continue;
			}

			candidate = fg_f;

			/* Flow in CA; it will do! */
			if (!(fg_f->f_state & FSE_FLOW_IN_FR))
				break;
		}

		fg->fg_coco = candidate;
		CTR3(KTR_FSE, "%s: new CoCo=%d (%p)", __func__,
		     (candidate != NULL) ? candidate->f_fi : -1, candidate);
	}

	f->f_state = FSE_FLOW_UNCOUPLED;
	CTR6(KTR_FSE, "%s(%p): flow %u decoupled from fg %p. CoCo=%d (%p)",
	     __func__, f, f->f_fi, fg, (fg->fg_coco != NULL) ? fg->fg_coco->f_fi : -1,
	     fg->fg_coco);
}

static struct fse_fg*
cc_fse_group(struct fse_flow *f)
{
	struct fse_fg *fg;
	struct fse_fg_head *fgt_head;
	struct tcpcb *tp;

	FSE_STATE_WLOCK_ASSERT(V_cc_fse_state);

	INP_LOCK_ASSERT(f->f_inp);
	tp = intotcpcb(f->f_inp);
	fg = cc_fse_lookup_addrs(f->f_inp->inp_faddr, f->f_inp->inp_laddr);
	if (fg == NULL) {
		fg = malloc(sizeof(struct fse_fg), M_FSE, M_NOWAIT|M_ZERO);

		if (fg == NULL) {
			CTR0(KTR_FSE,
			     "cc_fse_group: Could not allocate memory for new FSE FG!\n");
			return NULL;
		}

		rw_init_flags(&fg->fg_memberslock, "FSE FG lock", RW_RECURSE);
		LIST_INIT(&fg->fg_members);

		fgt_head = &V_cc_fse_state.fse_fgtable[ \
			FSE_FG_ADDRHASH(f->f_inp->inp_faddr.s_addr,
					f->f_inp->inp_laddr.s_addr,
					V_cc_fse_state.fse_fgtable_mask)];
		LIST_INSERT_HEAD(fgt_head, fg, fg_fgte);
	}

	FSE_FG_MBR_WLOCK(fg);
	LIST_INSERT_HEAD(&fg->fg_members, f, f_fge);
	fg->fg_membercount += 1;
	f->f_fg = fg;

	if (tp->t_hijackflags & TCP_HIJACK_ENABLED)
		cc_fse_couple(f);

	getmicrouptime(&fg->fg_lastupdate);
	FSE_FG_MBR_UNLOCK(fg);

	return fg;
}

struct fse_flow*
cc_fse_register(struct inpcb *inp)
{
	struct fse_flow *flow;
	struct tcpcb *tcb;

	flow = malloc(sizeof(struct fse_flow), M_FSE, M_NOWAIT|M_ZERO);

	if (flow == NULL)
		goto out;

	tcb = intotcpcb(inp);

	flow->f_p = tcb->t_fse_prio;
	flow->f_inp = inp;

	FSE_STATE_WLOCK(V_cc_fse_state);
	flow->f_fi = V_cc_fse_state.fse_next_fid++;

	/* The rest of the flow data MUST be set BEFORE grouping! */
	cc_fse_group(flow);
	CTR6(KTR_FSE, "cc_fse_register: new flow %u (%p), tcb=%p, fg=%p (%u/%u flows)",
	     flow->f_fi, flow, tcb, flow->f_fg, flow->f_fg->fg_num_coupled,
	     flow->f_fg->fg_membercount);
	FSE_STATE_UNLOCK(V_cc_fse_state);
 out:
	return flow;
}

void
cc_fse_deregister(struct fse_flow *f)
{
	struct fse_fg *fg;

	fg = f->f_fg;

	FSE_FG_MBR_WLOCK(fg);
	LIST_REMOVE(f, f_fge);
	fg->fg_membercount -= 1;
	cc_fse_decouple(f);
	getmicrouptime(&fg->fg_lastupdate);

	CTR6(KTR_FSE, "cc_fse_deregister: dropped flow %u (%p), tcb=%p, fg=%p (%u/%u flows)",
	     f->f_fi, f, intotcpcb(f->f_inp), f->f_fg, f->f_fg->fg_num_coupled,
	     f->f_fg->fg_membercount);
	/* XXX Empty FGs should be purged, but not immediately */
	FSE_FG_MBR_UNLOCK(fg);

	free(f, M_FSE);
}


/*
 * Update flow f's CC params via the FSE.
 * If f is uncoupled, leave it alone
 * If f is coupled, do the right thing to the ensemble.
 * Params set by the underlying CC is read out of TCB.
 * This function also takes care of (de)coupling as needed.
 */
void
cc_fse_update(struct fse_flow *f, uint32_t flags)
{
	struct tcpcb *tcb;
	struct fse_fg *fg;
	struct fse_flow *fg_f, *new_coco;
	u_long cc_cwnd, cc_ssthresh;

	KASSERT(f != NULL, ("updating a NULL flow"));
	INP_WLOCK_ASSERT(f->f_inp);

	tcb = intotcpcb(f->f_inp);
	fg = f->f_fg;
	cc_cwnd = tcb->snd_cwnd;
	cc_ssthresh = tcb->snd_ssthresh;
	CTR5(KTR_FSE, "%s(%p, %u) flow %u tcb=%p", __func__, f, flags,
	     f->f_fi, tcb);

	FSE_FG_MBR_WLOCK(fg);

	/* XXX kind of nasty to do these checks on every cwnd update */
	if ((f->f_state & FSE_FLOW_UNCOUPLED) &&
	    tcb->t_hijackflags & TCP_HIJACK_ENABLED) {
		cc_fse_couple(f);
		CTR3(KTR_FSE, "%s: Coupling TCB %p, now %d coupled flows.", __func__,
		     tcb, fg->fg_num_coupled);
	} else if ((f->f_state & FSE_FLOW_COUPLED) &&
		   !(tcb->t_hijackflags & TCP_HIJACK_ENABLED)) {
		cc_fse_decouple(f);
		CTR3(KTR_FSE, "%s: Decoupling TCB %p, now %d coupled flows.", __func__,
		     tcb, fg->fg_num_coupled);

	}

	if (f->f_state & FSE_FLOW_COUPLED) {

		/* Check if CC algo tried to go into SS */
		if (cc_cwnd < cc_ssthresh)
			f->f_state |= FSE_FLOW_WANTED_SS;
		else
			f->f_state &= ~FSE_FLOW_WANTED_SS;

		/* Check if flow is currently in FR */
		if (IN_RECOVERY(tcb->t_flags))
			f->f_state |= FSE_FLOW_IN_FR;
		else
			f->f_state &= ~FSE_FLOW_IN_FR;

		CTR6(KTR_FSE, "%s(%p, %u): coupled, st=0x%x, cc_cwnd=%u, cc_sst=%u\n",
		     __func__, f, flags, f->f_state, cc_cwnd, cc_ssthresh);

		if (fg->fg_coco != f) {
			if (IN_FASTRECOVERY(tcb->t_flags)) {
				/* Search for any other flows not in CA */
				int non_ca = 0;
				LIST_FOREACH(fg_f, &fg->fg_members, f_fge) {
					if (fg_f->f_state & FSE_FLOW_IN_FR) {
						non_ca = 1;
						break;
					}
				}

				/* Everyone else in CA, become CoCo */
				if (!non_ca) {
					fg->fg_coco = f;
					CTR2(KTR_FSE, "%s: FR flow %p taking over as CoCo.",
					     __func__, f);
				}
			} else {
				/* FIXME this is copy-pasta, REFACTOOOOOOR */
				f->f_cwnd = f->f_p * fg->fg_s_cwnd / fg->fg_s_p;
				if (fg->fg_s_ssthresh > 0)
					f->f_ssthresh = f->f_p * fg->fg_s_ssthresh / fg->fg_s_p;

				cc_fse_writeback(f);
				goto out;
			}
		}

		/* CoCo might have changed above */
		if (fg->fg_coco == f) {
			CTR6(KTR_FSE, "actual is CoCo; f_cwnd=%u, f_sst=%u, s_cwnd=%u, " \
			     "s_sst=%u, f_p=%u, s_p=%u.",
			     f->f_cwnd, f->f_ssthresh, fg->fg_s_cwnd, fg->fg_s_ssthresh,
			     f->f_p, fg->fg_s_p);
			if (cc_cwnd > cc_ssthresh && !IN_RECOVERY(tcb->t_flags)) {
				/* Normal CA update */
				if (cc_cwnd >= f->f_cwnd) /* AI... */
					fg->fg_s_cwnd += cc_cwnd - f->f_cwnd;
				else /* ..MD */
					/* XXX Operand order significant! */
					fg->fg_s_cwnd = fg->fg_s_cwnd * cc_cwnd / f->f_cwnd;

				f->f_cwnd = f->f_p * fg->fg_s_cwnd / fg->fg_s_p;
				f->f_ssthresh = cc_ssthresh;
				if (fg->fg_s_ssthresh > 0)
					f->f_ssthresh = f->f_p * fg->fg_s_ssthresh / fg->fg_s_p;

				cc_fse_writeback(f);
			} else if (IN_FASTRECOVERY(tcb->t_flags)) {
				fg->fg_s_ssthresh = fg->fg_s_cwnd / 2;
			} else if (f->f_state & FSE_FLOW_WANTED_SS) {
				/* if (flags & FSE_UPDATE_CONG_SIGNAL) { */
				/* Try to find a new CoCo not in SS */
				new_coco = NULL;
				LIST_FOREACH(fg_f, &fg->fg_members, f_fge) {
					if (!(fg_f->f_state & FSE_FLOW_WANTED_SS)) {
						new_coco = fg_f;
						break;
					}
				}

				/* XXX can be refactored with a goto
				 * out of the loop */
				if (new_coco == NULL) {
					/* Everyone in SS! */
					fg->fg_s_ssthresh = fg->fg_s_cwnd / 2;
					/* XXX Order of
					 * operands is
					 * significant here!!! */
					fg->fg_s_cwnd = fg->fg_s_cwnd * cc_cwnd / f->f_cwnd;
					CTR4(KTR_FSE, "%s: all SS - %u * %u / %u",
					     __func__, fg->fg_s_cwnd, cc_cwnd, f->f_cwnd);
					f->f_cwnd = f->f_p * fg->fg_s_cwnd / fg->fg_s_p;

					/* FIXME What about ssthresh?!
					 * Can't use writeback here?!*/
					tcb->snd_cwnd = max(f->f_cwnd, tcb->t_maxseg);
					CTR1(KTR_FSE, "%s: all flows in SS, aggregate " \
					     "dropping to SS", __func__);
				} else {
					fg->fg_coco = new_coco;
					CTR3(KTR_FSE, "%s: old coco in SS, elected %u (%p)",
					     __func__, new_coco->f_fi, new_coco);
					/* XXX Shouldn't we write
					 * back old values?
					 * NOT IN DRAFT */
					cc_fse_writeback(f);
				}
				/*}  else { */
				/* 	/\* Initial SS, let cwnd increase *\/ */
				/* 	fg->fg_s_cwnd += cc_cwnd - f->f_cwnd; */
				/* 	f->f_cwnd = f->f_p * fg->fg_s_cwnd / fg->fg_s_p; */
				/* } */
			}
		}
	}

 out:
	getmicrouptime(&fg->fg_lastupdate);
	FSE_FG_MBR_UNLOCK(fg);
}

/*
 * Remove and clean up after a FG.
 * Caller must hold both state and FG Wlock.
 * Releases the FG wlock.
 */
static void
cc_fse_fg_destroy(struct fse_fg *fg)
{
	FSE_STATE_WLOCK_ASSERT(V_cc_fse_state);
	FSE_FG_MBR_WLOCK_ASSERT(fg);
	KASSERT(fg->fg_membercount == 0,
		("Attempting to destroy non-empty FSE FG!"));

	/*
	 * There should be no race wrt. threads being blocked on this
	 * lock at this point, they would need a pointer via some
	 * flow, and we just checked there were none in this FG. No
	 * new ones can be added while we hold the state lock.
	 */
	FSE_FG_MBR_UNLOCK(fg);

	LIST_REMOVE(fg, fg_fgte);

	rw_destroy(&fg->fg_memberslock);

	free(fg, M_FSE);
}

/*
 * Walk the FG hashtable and reap stale empty FGs.
 * Called by periodic callout.
 */
static void
cc_fse_reaper(void *arg)
{
	struct fse_fg *fg, *fg_temp;
	struct timeval now;
	int i;

	CURVNET_SET((struct vnet *) arg);

	getmicrouptime(&now);

	FSE_STATE_WLOCK(V_cc_fse_state);
	for (i = 0; i < V_cc_fse_state.fse_fgtable_mask; i++) {
		LIST_FOREACH_SAFE(fg, &V_cc_fse_state.fse_fgtable[i], fg_fgte, fg_temp) {
			/*
			 * XXX If we get smarter about FG locking, it
			 * would make sense to check under Rlock and
			 * upgrade to Wlock only if we need to reap.
			 */
			FSE_FG_MBR_WLOCK(fg);
			if (fg->fg_membercount == 0
			    && (now.tv_sec - fg->fg_lastupdate.tv_sec > V_cc_fse_reaper_limit)) {
				cc_fse_fg_destroy(fg);
				/* fg_destroy cleans the lock: */
				continue;
			}
			FSE_FG_MBR_UNLOCK(fg);
		}
	}
	FSE_STATE_UNLOCK(V_cc_fse_state);

	callout_reset(&V_cc_fse_reaper_callout, V_cc_fse_reaper_ival * hz,
		      cc_fse_reaper, curvnet);

	CURVNET_RESTORE();
}

/*
 * Get or set the default priority for this VNET.
 * Needs a handler procedure due to input validation.
 */
static int
cc_fse_default_prio_handler(SYSCTL_HANDLER_ARGS)
{
	int error = 0;
	int value = V_cc_fse_default_prio;

	error = sysctl_handle_int(oidp, &value, 0, req);

	if (error != 0)
		return error;

	if (value < 1 || value > 255 || value % 2)
		return EINVAL;

	V_cc_fse_default_prio = value;

	return 0;
}

/* Init hook */
VNET_SYSINIT(cc_fse, SI_SUB_LAST, SI_ORDER_ANY, cc_fse_init, NULL);

/* Define sysctl subtree for our config */

SYSCTL_NODE(_net_inet_tcp_cc, OID_AUTO, fse, CTLFLAG_RW, NULL,
	    "Flow State Exchange");

SYSCTL_INT(_net_inet_tcp_cc_fse, OID_AUTO, fgbuckets, CTLFLAG_RDTUN | CTLFLAG_VNET,
	   &VNET_NAME(cc_fse_fgbuckets), 0, "FG hashtable buckets");

SYSCTL_INT(_net_inet_tcp_cc_fse, OID_AUTO, reaper_ival, CTLFLAG_RW | CTLFLAG_VNET,
	   &VNET_NAME(cc_fse_reaper_ival), 0, "FG reaper interval");

SYSCTL_INT(_net_inet_tcp_cc_fse, OID_AUTO, reaper_limit, CTLFLAG_RW | CTLFLAG_VNET,
	   &VNET_NAME(cc_fse_reaper_limit), 0, "FG idle timeout");

SYSCTL_PROC(_net_inet_tcp_cc_fse, OID_AUTO, default_prio,
	    CTLFLAG_RW | CTLFLAG_VNET | CTLTYPE_UINT, NULL, 0,
	    cc_fse_default_prio_handler, "UI", "default priority");
