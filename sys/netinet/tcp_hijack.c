/*-
 * Copyright (c) 2015 Kristian A. Hiorth <kah@kahnews.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#include "opt_inet.h"

#include <sys/param.h>
#include <sys/kernel.h>
#include <sys/kthread.h>
#include <sys/libkern.h>
#include <sys/malloc.h>
#include <sys/mbuf.h>
#include <sys/protosw.h>
#include <sys/socket.h>
#include <sys/socketvar.h>
#include <sys/sysctl.h>
#include <sys/ktr.h>

#include <net/pfil.h>

#include <netinet/in.h>
#include <netinet/in_pcb.h>
#include <netinet/ip.h>
#include <netinet/ip_var.h>
#include <netinet/tcp_var.h>
#include <netinet/tcp_fsm.h>
#include <netinet/udp.h>
#include <netinet/udp_var.h>

#include <netinet/tcp_hijack.h>

VNET_DEFINE(uint32_t, tcp_hijack_enabled) = 0;
VNET_DEFINE(uint32_t, tcp_hijack_port) = 1021; /* exp.1 per RFC4727 */
VNET_DEFINE(struct socket *, tcp_hijack_tsock) = NULL;
VNET_DEFINE(struct tcp_tiumap_info, tcp_tiumapinfo);
VNET_DEFINE(uint32_t, tcp_tiumaxretries) = 2;
VNET_DEFINE(uint32_t, tcp_hijack_default_disabled) = 0;

static VNET_DEFINE(struct pfil_head, tiu_pfil_hook);
#define	V_tiu_pfil_hook	VNET(tiu_pfil_hook)

/*
 * Allocate a free flow id for this (src addr, dst addr) pair, if
 * available.
 * Returns the flowId on success, -1 on failure.
 */
static int
tcp_hijack_nflow(struct tcp_tiumap *map)
{
	struct tcp_tiu_conn *pp;
	int count = TCPINUDP_MAXFLOWS;
        uint8_t flowid;

	TCP_TIUMAP_WLOCK_ASSERT(map);

	flowid = map->tum_lastflow;

	do {
		flowid = (flowid + 1) % TCPINUDP_MAXFLOWS;
		pp = &map->tum_flow[flowid];

		if (pp->lport == 0) {
			/* Caller must claim the flowId by setting
			 * addresses BEFORE dropping wlock! */
			map->tum_lastflow = flowid;

			return flowid;
		}

		count--;
	} while (count > 0); 	/* XXX did I do an offbyone here? */

	return -1;
}

/*
 * Do a lookup in the port map based on the {src, dst} addresses.
 */
static struct tcp_tiumap *
tcp_tiumap_lookup(struct in_addr faddr, struct in_addr laddr)
{
	struct tcp_tiumap_head *head;
	struct tcp_tiumap *map;

	TCP_TIUMAPINFO_LOCK_ASSERT(V_tcp_tiumapinfo);

	/*
	 * XXX We don't take the map entry lock because an entry will
	 * never have its address info changed after creation. Hence,
	 * mapinfo lock is sufficient.
	 */
	head = &V_tcp_tiumapinfo.tmi_map[TCP_TIUMAPHASH(faddr.s_addr, laddr.s_addr,
			V_tcp_tiumapinfo.tmi_hashmask)];
	LIST_FOREACH(map, head, tum_mapentry) {
		if (map->tum_laddr.s_addr == laddr.s_addr
		    && map->tum_faddr.s_addr == faddr.s_addr)
			return map;
	}

	return NULL;
}

/*
 * Allocate a new map entry and initialize it with the supplied addresses.
 * Returns new map entry, NULL if OOM.
 * Always unlocks mapinfo!
 */
static struct tcp_tiumap *
tcp_newtiumap(struct in_addr faddr, struct in_addr laddr)
{
	struct tcp_tiumap *map, *check;
	struct tcp_tiumap_head *head;

	map = uma_zalloc(V_tcp_tiumapinfo.tmi_mapzone, M_NOWAIT | M_ZERO);

	if (map == NULL)
		goto out;

	/* XXX A bit unsure if this works with IPv6 or not? */
	bcopy(&faddr, &map->tum_faddr, sizeof(struct in_addr));
	bcopy(&laddr, &map->tum_laddr, sizeof(struct in_addr));
	rw_init(&map->tum_entrylock, "TCP-in-UDP map entry lock");

	TCP_TIUMAPINFO_RLOCK_ASSERT(V_tcp_tiumapinfo);
	if (TCP_TIUMAPINFO_TRY_UPGRADE(V_tcp_tiumapinfo) == 0) {
		/* Someone else is reading, we have to drop and claim wlock */
		TCP_TIUMAPINFO_RUNLOCK(V_tcp_tiumapinfo);
		TCP_TIUMAPINFO_WLOCK(V_tcp_tiumapinfo);

		/*
		 * Must recheck that the map isn't set, someone might
		 * have created it between dropping rlock and getting
		 * wlock!
		 */
		check = tcp_tiumap_lookup(faddr, laddr);
		if (check != NULL) {
			uma_zfree(V_tcp_tiumapinfo.tmi_mapzone, map);
			rw_destroy(&map->tum_entrylock);
			map = check;
			goto out;
		}
	}

	head = &V_tcp_tiumapinfo.tmi_map[TCP_TIUMAPHASH(map->tum_faddr.s_addr,
							map->tum_laddr.s_addr,
							V_tcp_tiumapinfo.tmi_hashmask)];
	LIST_INSERT_HEAD(head, map, tum_mapentry);

 out:
	TCP_TIUMAPINFO_UNLOCK(V_tcp_tiumapinfo);
	return map;
}

/*
 * Return a valid map for the supplied addresses. Creates a new one if
 * necessary.
 * Returns NULL if map neither exists nor could be created due to OOM.
 */
static __inline struct tcp_tiumap*
tcp_grabtiumap(struct in_addr faddr, struct in_addr laddr)
{
	struct tcp_tiumap *map;

	TCP_TIUMAPINFO_RLOCK(V_tcp_tiumapinfo);
	map = tcp_tiumap_lookup(faddr, laddr);

	if (map == NULL) {
		/* returns mapinfo unlocked: */
		map = tcp_newtiumap(faddr, laddr);
	} else
		TCP_TIUMAPINFO_RUNLOCK(V_tcp_tiumapinfo);

	return map;
}

/*
 * Called when a TCP connection is set up.  Allocate a channel
 * (flowid) for this connection.
 *
 * Returns allocated flow ID on * success, 0xff on error.
 */
int
tcp_hijack_connect(struct tcpcb *tp)
{
	/* int error = 0; */
	uint8_t flowid;
	struct tcp_tiumap *map;

	INP_LOCK_ASSERT(tp->t_inpcb);

	map = tcp_grabtiumap(tp->t_inpcb->inp_faddr, tp->t_inpcb->inp_laddr);
	if (map == NULL)
		return TCPINUDP_INVALFLOWID; /* Give up */

	TCP_TIUMAP_WLOCK(map);
	flowid = tcp_hijack_nflow(map);

	if (flowid < 0) {
		flowid = TCPINUDP_INVALFLOWID;
		goto out;
	}

	map->tum_flow[flowid].lport = tp->t_inpcb->inp_lport;
	map->tum_flow[flowid].fport = tp->t_inpcb->inp_fport;
	map->tum_flow[flowid].tp = tp;
	tp->t_tiucb->tiu_flowid = flowid;
	tp->t_hijackflags |= TCP_HIJACK_OFFERING;

 out:
	TCP_TIUMAP_WUNLOCK(map);
	return flowid;
}


/*
 * Attempt to reserve a given flow on the connection specified by inc.
 * Returns 0 on success.
 */
int
tcp_hijack_reserveflow(struct in_conninfo *inc, uint8_t flow)
{
	struct tcp_tiumap *map;
	int error = 0;

	KASSERT(flow <= TCPINUDP_MAXFLOWID, ("tcp_hijack_reserveflow: flowid out of range."));

	map = tcp_grabtiumap(inc->inc_faddr, inc->inc_laddr);
	if (map == NULL)
		return 1;

	TCP_TIUMAP_WLOCK(map);
	if (map->tum_flow[flow].lport != 0) {
		error = 1;
		goto out;
	}

	map->tum_flow[flow].lport = inc->inc_lport;
	map->tum_flow[flow].fport = inc->inc_fport;

 out:
	TCP_TIUMAP_WUNLOCK(map);
	return error;
}

/*
 * Remove an entry from the global flow map. Expects exclusive map
 * info lock.
 */
static __inline void
tcp_hijack_killmap(struct tcp_tiumap *map)
{
	TCP_TIUMAPINFO_WLOCK_ASSERT(V_tcp_tiumapinfo);

	LIST_REMOVE(map, tum_mapentry);
}

/*
 * Drop a flowid to port pair mapping.  Will free the whole map if
 * this was the only entry in it.  Expects exclusive map lock and
 * shared mapinfo lock.  Map lock will always be released, mapinfo
 * lock will be returned upgraded to exclusive lock.
 */

static __inline void
tcp_hijack_unmap(struct tcp_tiumap *map, uint8_t flow)
{
	int flowid;
	TCP_TIUMAP_WLOCK_ASSERT(map);
	TCP_TIUMAPINFO_LOCK_ASSERT(V_tcp_tiumapinfo);

	memset(&map->tum_flow[flow], 0, sizeof(struct tcp_tiu_conn));

	/* Check if this map actually contains anything now */
	if (!TCP_TIUMAPINFO_TRY_UPGRADE(V_tcp_tiumapinfo)) {
		TCP_TIUMAPINFO_UNLOCK(V_tcp_tiumapinfo);
		TCP_TIUMAPINFO_WLOCK(V_tcp_tiumapinfo);
		/*
		 * XXX If something happened to map between the locks,
		 * that is fine; we will react appropriately to it
		 * below.
		 */
	}

	for (flowid = 0; flowid < TCPINUDP_MAXFLOWS; flowid++) {
		if (map->tum_flow[flowid].lport != 0) {
			TCP_TIUMAP_UNLOCK(map);
			return;
		}
	}

	/* Map is empty, delist and release it */
	tcp_hijack_killmap(map);
	TCP_TIUMAP_UNLOCK(map);

	uma_zfree(V_tcp_tiumapinfo.tmi_mapzone, map);
}

/*
 * Drop a flow associated with an existing tcpcb.
 * Mapinfo lock must be held!
*/
static void
tcp_hijack_dropflow(struct tcp_tiumap *map, struct tcpcb *tp)
{
	TCP_TIUMAP_WLOCK_ASSERT(map);
	TCP_TIUMAPINFO_LOCK_ASSERT(V_tcp_tiumapinfo);

	tcp_hijack_unmap(map, tp->t_tiucb->tiu_flowid);
}

/* Drop a flow by full lookup, used by syncache. */
void
tcp_hijack_releaseflow(struct in_conninfo *inc, uint8_t flow)
{
	struct tcp_tiumap *map;

	TCP_TIUMAPINFO_RLOCK(V_tcp_tiumapinfo);
	map = tcp_tiumap_lookup(inc->inc_faddr, inc->inc_laddr);

	if (map == NULL)
		goto out;

	TCP_TIUMAP_WLOCK(map);

	tcp_hijack_unmap(map, flow);
	/* Map returned unlocked */
 out:
	TCP_TIUMAPINFO_UNLOCK(V_tcp_tiumapinfo);
}

/* 
 * Called when a TCP connection is tore down
 */
void
tcp_hijack_disconnect(struct tcpcb *tp)
{
	struct tcp_tiumap *map;
	/* FIXME reconsider flag */
	KASSERT(tp->t_hijackflags & TCP_HIJACK_ACTIVE,
		("tcp_hijack_disconnect: called on untunneled tcb"));

	TCP_TIUMAPINFO_RLOCK(V_tcp_tiumapinfo);
	map = tcp_tiumap_lookup(tp->t_inpcb->inp_faddr, tp->t_inpcb->inp_laddr);

	/*
	 * XXX map should never be NULL here, but there was a race
	 * condition in map creation that caused this to happen. Guard
	 * against regressions:
	 */
	KASSERT(map != NULL, ("%s: NULL map for tp=%p", __func__, tp));

	TCP_TIUMAP_WLOCK(map);
	tcp_hijack_dropflow(map, tp);
	/* Map lock released by _dropflow */

	TCP_TIUMAPINFO_UNLOCK(V_tcp_tiumapinfo);
}

/* 
 * Check whether a given TCP connection has been hijacked, ie. if
 * there is an active tunnel for it.
 *
 */
int
tcp_hijacked(struct tcpcb *tp)
{
	return (0);
}

/* 
 * Do TCP header compression (remove unused/unneccesary parts).
 * The TCP header is expected to be at m->m_data.
 * If rewrite is non-zero, full compression takes place. Otherwise,
 * only the reordering of the {source port, dest port} and {offset,
 * reserved, flags, window size} fields is done.
 * Zeroed rewrite implies to set magic offset value to indicate a setup segment.
 *
 * IMPORTANT: there must be no segment payload in the first mbuf!
 */
static __inline void
tcp_hijack_thcomp(struct mbuf *m, uint8_t flowid, int rewrite)
{
	struct tcphdr *th;
	uint8_t swapb[4]; 	/* For reordering FIXME magic number? */

	th = mtod(m, struct tcphdr *);

	CTR5(KTR_TCPINUDP, "tcp_hijack_thcomp(%p): th=%p m_len=%d dst=%p src=%p\n",
	     m, th, m->m_len, &(th->th_sum), th + 1);
	
	if (rewrite) {
		/* Encode flowid (4 highest bits into reserved bits, lowest
		 * bit into URG flag) */
		th->th_x2 = (flowid >> 1); /* 4 highest bits */
		/* lowest bit; can I make a bitwise expression? */
		if (flowid & 1) {
			th->th_flags |= TH_URG;
		} else {
			th->th_flags &= ~TH_URG;
		}

		/* Reorder offset, overwriting ports */
		bcopy(TCPHDR_OFF(th), &th->th_sport, sizeof(swapb));

		/* Overwrite Urgent Pointer + Checksum fields by moving up
		 * whatever comes after (options) */
		if (m->m_len > sizeof(struct tcphdr)) {
			bcopy(th + 1, TCPHDR_OFF(th), m->m_len - sizeof(struct tcphdr));
		}

		m->m_len -= 8; 	/* Length of cksum + urgptr + {s,d}ports fields */
		m->m_pkthdr.len -= 8;
	} else {
		th->th_off = TCPINUDP_SETUPSEGMENT;

		/* Swap ports and offset... */
		bcopy(&th->th_sport, swapb, sizeof(swapb));
		bcopy(TCPHDR_OFF(th), &th->th_sport, sizeof(swapb));
		bcopy(swapb, TCPHDR_OFF(th), sizeof(swapb));
	}		
}

/* 
 * Divert output through tunnel.
 * Triggers header compression if there is a PCB and state is
 * established.
 */
int
tcp_hijack_output(struct mbuf *m, struct mbuf *opt, struct route *ro, int flags,
		  struct ip_moptions *imo, struct inpcb *inp)
{
	int error = 0;
	struct ip *ip = NULL;
	struct in_addr src, dst;
	struct sockaddr_in sin;
	struct tcpcb *tp = NULL;
	uint8_t flowid = TCPINUDP_INVALFLOWID;

	if (!V_tcp_hijack_enabled) {
		return ENETDOWN;
	}

	if (inp != NULL) {
		tp = intotcpcb(inp);
		if (tp != NULL)
			flowid = tp->t_tiucb->tiu_flowid;
		/*
		 * FIXME might end up with bad flowid here because of
		 * RST drops. Maybe check for it and NULL tp if so?
		 * On second thought, it probably doesn't matter since
		 * in this case we will never _use_ the flowid...
		 */
	}

	if (!PFIL_HOOKED(&V_tiu_pfil_hook))
		goto rewrite;

	if (pfil_run_hooks(&V_tiu_pfil_hook, &m, NULL, PFIL_OUT, inp)) {
		CTR0(KTR_TCPINUDP, "tcp_hijack_output: pfil ate our packet!\n");
		return IPPROTO_DONE;
	}

 rewrite:
	/* Copy out src and dst addresses from IP header, then rip it off */
	/* FIXME could perhaps avoid all this copying and recreating
	 * structs by placing a readymade sockaddr_in in the TuI CB
	 * in the respective TCB */
	ip = mtod(m, struct ip *);
	src = ip->ip_src;
	dst = ip->ip_dst;
	m_adj(m, sizeof(struct ip));

	memset(&sin, 0, sizeof(struct sockaddr_in));
	sin.sin_family = AF_INET;
	sin.sin_port = htons(V_tcp_hijack_port);
	sin.sin_addr = dst;
	sin.sin_len = sizeof(struct sockaddr_in);

	CTR3(KTR_TCPINUDP, "****\ntcp_hijack_output: precompr: m_len=%d" \
	     "p_len=%d m_len()=%d\n", m->m_len, m->m_pkthdr.len,
	     m_length(m, NULL));
	CTR1(KTR_TCPINUDP, "tcp_hijack_output: flowid=%hhu\n", flowid);

	/* TODO: doublecheck tunnel state first */
	/* FIXME possible the state check will screw us in the case
	 * where the 3rd handshake packet (ACK) contains data, which
	 * is legal */
	tcp_hijack_thcomp(m, flowid, (tp != NULL && tp->t_state >= TCPS_ESTABLISHED));

	CTR4(KTR_TCPINUDP, "\ntcp_hijack_output: postcompr: m_len=%d p_len=%d" \
	     "m_len()=%d\nstate=%d\n++++\n\n",
	     m->m_len, m->m_pkthdr.len, m_length(m, NULL),
	     (tp != NULL ? tp->t_state : -1));


	error = udp_usrreqs.pru_send(V_tcp_hijack_tsock, 0, m, (struct sockaddr *)&sin,
				     NULL, curthread);
	CTR1(KTR_TCPINUDP, "tcp_hijack_output: UDP returned %d\n", error);
	return (error);
}

/* 
 * Do TCP header decompression (inflate to normal size)
 * m is the head of the mbuf chain, expecting IP header of length off
 * at m->m_data. Will pullup TCP header as required.
 * Returns the number of bytes added, -1 if there was a problem.
 * flowid is set to the encoded flowId.
 */
static __inline int
tcp_hijack_thinfl(struct mbuf **mp, int off, uint8_t *flowid, int *rewritten)
{
	struct tcphdr *th;
	struct mbuf *m = *mp;
	caddr_t ip;
	int tcplen, expectedsize;
	uint8_t swapb[4];

	CTR4(KTR_TCPINUDP, "tcp_hijack_thinfl(%p, %d): m(orig)=%p m_len(orig)=%d\n",
	     mp, off, m, m->m_len);

	ip = mtod(m, caddr_t);

	/* TCP header might be in next mbuf... */
	if (m->m_len < off + 1) {
		/* FIXME what if it's in a cluster? argh */
		/* Check if there is actually another (filled) mbuf */
		if (m->m_next == NULL || m->m_len < 1)
			return -1;
		th = (struct tcphdr *)(m->m_next->m_data);
	} else
		th = (struct tcphdr *)(ip + off);

	if (TCPHDR_REWRITTENOFF(th) == TCPINUDP_SETUPSEGMENT) {
		*rewritten = 0;
		expectedsize = sizeof(struct tcphdr);
	} else {
		*rewritten = 1;
		/* -8 because we compress by that much (TODO get rid of magic number!) */
		expectedsize = sizeof(struct tcphdr) - 8;
	}

	CTR2(KTR_TCPINUDP, "tcp_hijack_thinfl: raw_off=%u, rewritten=%d\n",
	     TCPHDR_REWRITTENOFF(th), *rewritten);

	/*
	 * Pull up TCP header, which might be in the next mbuf.
	 * This will save an m_pullup in tcp_input when that is the
	 * case.
	 */
	/* FIXME what about options? */
	if ((m = m_pullup(m, expectedsize + off)) == NULL) {
		/* Ouch, this was a bad packet. m_pullup freed m for us. */
		return -1;
	}

	/* In case there was a realloc in m_pullup we have to do this: */
	*mp = m;
	ip = mtod(m, caddr_t);
        th = (struct tcphdr *)(ip + off);

	/* length of TCP segment present in first mbuf */
	tcplen = m->m_len - off;

	if (*rewritten) {
		/* Move TCP opts + anything after down 8 bytes */
		/* FIXME check that we aren't pushing things "off the end" of
		 * the mbuf!
		 * Also get rid of the damn magic number, spent a week
		 * before noticing + was - in len here!
		 */
		if (m->m_len > (expectedsize + off)) {
			bcopy(TCPHDR_OFF(th), th + 1, tcplen - sizeof(struct tcphdr) + 8);
		}
		memset(&(th->th_sum), 0, 4); /* Zero out checksum  + urgentptr */
		m->m_len += 8; 	/* Length of inflated fields */
		m->m_pkthdr.len += 8;

		/* Put offset and friends back where TCP expects them */
		bcopy(&th->th_sport, TCPHDR_OFF(th), sizeof(swapb));

		*flowid = th->th_x2 << 1;
		if (th->th_flags & TH_URG) {
			*flowid +=1;
			th->th_flags &= ~TH_URG;
		}

		if (*flowid > TCPINUDP_MAXFLOWID) {
			m_freem(m);
			*mp = NULL;
			return -1;
		}
	} else {
		/* Swap ports and offset etc */
		bcopy(TCPHDR_OFF(th), swapb, sizeof(swapb));
		bcopy(&th->th_sport, TCPHDR_OFF(th), sizeof(swapb));
		bcopy(swapb, &th->th_sport, sizeof(swapb));

		/* Recalculate what offset should be */
		/*
		 * NB! Uses length of *whole* mbuf chain, we assume
		 * there won't be any data anyway when using the
		 * magic offset
		 */
		th->th_off = (m_length(m, NULL) - off) / 4;
	}

	CTR5(KTR_TCPINUDP, "m(new)=%p, tcplen=%d, th=%p, ip=%p, m_len(new)=%d\n",
	     m, tcplen, th, ip, m->m_len);

	/* FIXME so nasty magical numbers... */
	return (*rewritten ? 8 : 0);
}

/*
 * Do TCP-in-UDP input processing, rewriting and deflating the segment
 * so that it may be processed by TCP. Will look up the flow id in the
 * map to replace the correct port numbers.
 * Locks should not be held when calling this function, will lock what
 * it needs itself.
 */
int
tcp_hijack_input(struct mbuf **mp, int *offp, int proto)
{
	int error = 0;
	struct mbuf *m = *mp;
	caddr_t ip; 		/* We want to do ptr arithmetic by
				 * the byte with these */
	caddr_t ip_orig;
	struct ip *ip_hdr;
	struct tcphdr *th;
	int inflated;
	int off = *offp; 	/* This will be set to the IP hdr
				 * length */
	uint8_t flowid;
	int rewritten;
	struct tcp_tiumap *map = NULL;
	struct tcp_tiumtag *tag;
	struct inpcb *inp = NULL;

	CTR3(KTR_TCPINUDP, "xxxx\ntcp_hijack_input(%p, %d, %d)\n", *mp,
	     *offp, proto);

	/* Relocate IP header, overwriting UDP header */

	ip_orig = mtod(m, caddr_t);

	CTR3(KTR_TCPINUDP, "ip_orig=%p, sz(s uh)=%lu, sz(s ip)=%lu ",
	     ip_orig, sizeof(struct udphdr), sizeof(struct ip));
	
	ip = (ip_orig + sizeof(struct udphdr));
	CTR2(KTR_TCPINUDP, "ip=%p src=%p\n", ip, ip_orig);
	memmove(ip, ip_orig, off);
	m->m_data = ip;
	m->m_len -= sizeof(struct udphdr);
	m->m_pkthdr.len -= sizeof(struct udphdr);

	CTR3(KTR_TCPINUDP, "\ntcp_hijack_input: preinfl: m_len=%d p_len=%d m_len()=%d\n",
	     m->m_len, m->m_pkthdr.len, m_length(m, NULL));

	/* Inflate the TCP header to be as normal */
	inflated = tcp_hijack_thinfl(mp, off, &flowid, &rewritten);
	if (inflated == -1) {
		/* mbuf will have been freed in _thinfl */
		CTR0(KTR_TCPINUDP,
		     "tcp_hijack_input: inflater could not rewrite header;abort\n----\n");
		error = IPPROTO_DONE;
		*mp = NULL;

		return (error);
	}
	
	/* Fix IP total length field to reflect the removed UDP header: */
	/* old m might no longer be valid after decompression */
	m = *mp;
	ip_hdr = mtod(m, struct ip *);
	ip_hdr->ip_len = htons(ntohs(ip_hdr->ip_len) - sizeof(struct udphdr) + inflated);
	ip_hdr->ip_p = IPPROTO_TCP; /* Or else *_respond get confused */

	if (rewritten) {
		/* Set ports based on flowid lookup */
		TCP_TIUMAPINFO_RLOCK(V_tcp_tiumapinfo);
		map = tcp_tiumap_lookup(ip_hdr->ip_src, ip_hdr->ip_dst);
		TCP_TIUMAPINFO_RUNLOCK(V_tcp_tiumapinfo);

		if (map == NULL) {
			CTR0(KTR_TCPINUDP,
			     "tcp_hijack_input: did not find portmap entry\n----\n");
			m_freem(m);
			*mp = NULL;
			return IPPROTO_DONE; /* Is this right? */
			/* FIXME should we send some explicit error instead? */
		}

		th = (struct tcphdr *)(ip_hdr + 1);

		TCP_TIUMAP_RLOCK(map);
		if (map->tum_flow[flowid].fport == 0) {
			TCP_TIUMAP_RUNLOCK(map);
			m_freem(m);
			*mp = NULL;
			CTR1(KTR_TCPINUDP,
			     "tcp_hijack_input: got segment with unmapped flowid=%hhu\n" \
			     "----\n", flowid);
			return IPPROTO_DONE;
		}
		th->th_sport = map->tum_flow[flowid].fport;
		th->th_dport = map->tum_flow[flowid].lport;
		if (map->tum_flow[flowid].tp != NULL)
			inp = map->tum_flow[flowid].tp->t_inpcb;
		TCP_TIUMAP_RUNLOCK(map);
	}
	
	CTR3(KTR_TCPINUDP, "\ntcp_hijack_input: postinfl: m_len=%d p_len=%d m_len()=%d\n",
	     m->m_len, m->m_pkthdr.len, m_length(m, NULL));
	CTR2(KTR_TCPINUDP, "tcp_hijack_input: flowid=%hhu, rewritten=%d\n----\n\n",
	     flowid, rewritten);

	/* 
	 * Use the hardware-offloaded checksumming flags to convince
	 * TCP that the checksum was okay.
	 */
	m->m_pkthdr.csum_flags |= (CSUM_DATA_VALID | CSUM_PSEUDO_HDR);
	m->m_pkthdr.csum_data = 0xffff; /* Magic value */

	tag = (struct tcp_tiumtag *)m_tag_alloc(TCP_TIUMTAG_COOKIE, TCP_TIUMTAG,
						sizeof(struct tcp_tiumtag) -
						sizeof(struct m_tag), M_NOWAIT);
	/*
	 * We need tags to work out states on conn. establishment,
	 * drop if we can't tag
	 */
	/* TODO maybe only drop if relevant flags are set? */
	if (tag != NULL) {
		tag->tiu_map = map;
		m_tag_prepend(m, (struct m_tag *)tag);
	} else {
		CTR0(KTR_TCPINUDP, "tcp_hijack_input: could not allocate mtag, dropping.\n");
		return IPPROTO_DONE;
	}

	if (!PFIL_HOOKED(&V_tiu_pfil_hook))
		goto passup;

	/* SIFTR needs a read lock: */
	if (inp != NULL)
		INP_RLOCK(inp);
	if (pfil_run_hooks(&V_tiu_pfil_hook, &m, NULL, PFIL_IN, inp)) {
		CTR0(KTR_TCPINUDP, "tcp_hijack_input: pfil ate our packet!\n");
		return IPPROTO_DONE;
	}
	if (inp != NULL)
		INP_RUNLOCK(inp);

 passup:
	error = tcp_input(mp, &off, IPPROTO_TCP);

	return (error);
	/* TODO refactor error exits? */
}

/*
 * Process control inputs that happen on the tunnel socket
 * (ie. ICMP messages).
 */
void
tcp_hijack_ctlinput(struct ip *ip, int error)
{
	struct tcp_tiumap *map;
	struct tcpcb *tp;
	struct in_conninfo inc;
	int f;

	CTR1(KTR_TCPINUDP, "tcp_hijack_ctlinput: got ICMP error=%d", error);

	switch (error) {
	case ECONNREFUSED:
		/* Disable tunnel for any connection to this host */

		/*
		 * Mark host TuI-unable, do it first to avoid further
		 * attempts while we are fixing state here.
		 */

		/* XXX we don't care about the rest of the struct, hc
		 * only needs a foreign addr to hash */

		inc.inc_faddr = ip->ip_dst;
		tcp_hc_update_tcpinudp(&inc, TCP_HIJACK_DISABLED);


		/* Grab write lock immediately, we'd have to recheck
		 * this otherwise. */
		TCP_TIUMAPINFO_WLOCK(V_tcp_tiumapinfo);

		/* The IP header is the "returned" header from ICMP
		 * here, so dst = foreign and src = local! */
		map = tcp_tiumap_lookup(ip->ip_dst, ip->ip_src);

		if (map == NULL) {
			CTR0(KTR_TCPINUDP, "tcp_hijack_ctlinput: no matching map\n");
			TCP_TIUMAPINFO_RUNLOCK(V_tcp_tiumapinfo);
			return;
		}

		TCP_TIUMAP_WLOCK(map);

		/* Remove mappings for this host-pair from global list */
		tcp_hijack_killmap(map);
		/*
		 * XXX We drop both locks immediately to avoid LORs
		 * with the INP lock
		 */
		TCP_TIUMAP_UNLOCK(map);
		TCP_TIUMAPINFO_UNLOCK(V_tcp_tiumapinfo);


		/*
		 * We bulk-disconnect all/any flows on this map here,
		 * instead of calling _disconnect since we no longer
		 * need to lock on the mapping lock, but do need to
		 * lock each INP lock.
		 */
		for (f = 0; f < TCPINUDP_MAXFLOWS; f++)
			if (map->tum_flow[f].tp != NULL) {
				tp = map->tum_flow[f].tp;
				KASSERT(tp->t_inpcb != NULL, \
					("%s: Mapped TiU TCB is corrupt. m=%p f=%d" \
					 " (lp=%hd, fp=%hd) tp=%p", __func__, map, f, \
					 map->tum_flow[f].lport, map->tum_flow[f].fport, tp));

				map->tum_flow[f].lport = 0;

				INP_WLOCK(tp->t_inpcb);
				tp->t_hijackflags &=
					~(TCP_HIJACK_ACTIVE | TCP_HIJACK_PROBING);
				tp->t_hijackflags |=
					TCP_HIJACK_DISABLED;
				INP_WUNLOCK(tp->t_inpcb);
			}

		uma_zfree(V_tcp_tiumapinfo.tmi_mapzone, map);

		/* XXX Could trigger immediate TCP retransmit; need to
		 * tweak params to remove rxmit count, reset timers
		 * etc. Actually, this is needlessly complicated in
		 * the "race" model with parallel setup
		 */
		break;
	case EHOSTUNREACH:
		/*
		 * TODO resolve inp - problem: we have no info about
		 * which TCP connection triggered the error, would
		 * need to blindly notify anything that matches the
		 * address pair
		 * TCP mostly ignores this error anyway, not sure it
		 * is worth the trouble and lookup overhead.
		 */
		break;
	}
}

/*
 * Initialize TCP-in-UDP state, set up tunnel socket etc.
 */
static void
tcp_hijack_init(void)
{
	int error = 0;

	if (!V_tcp_hijack_enabled) {
		goto out;
	}

	/* Set up tunnel listener */
	struct socket *lsock = NULL;
	struct sockaddr_in sin;

	CTR0(KTR_TCPINUDP, "TCP-in-UDP initializing...\n");

	/* Initialize packet filter hooks. */
	V_tiu_pfil_hook.ph_type = PFIL_TYPE_AF;
	V_tiu_pfil_hook.ph_af = AF_INET_TIU;
	if ((error = pfil_head_register(&V_tiu_pfil_hook)) != 0)
		printf("%s: WARNING: unable to register pfil hook, "
		       "error %d\n", __func__, error);

	error = socreate(PF_INET, &lsock, SOCK_DGRAM, 0, curthread->td_ucred,
			 curthread);
	if (error) {
		CTR1(KTR_TCPINUDP, "tcp_hijack_init: socreate: %d\n", error);
		goto out;
	}

	memset(&sin, 0, sizeof(struct sockaddr_in));
	sin.sin_len = sizeof(struct sockaddr_in);
	sin.sin_family = AF_INET;
	sin.sin_port = htons(V_tcp_hijack_port);
	sin.sin_addr.s_addr = htonl(INADDR_ANY);

	error = sobind(lsock, (struct sockaddr *) &sin, curthread);
	if (error) {
		CTR1(KTR_TCPINUDP, "tcp_hijack_init: sobind: %d\n", error);
		goto cleanup;
	}

	V_tcp_hijack_tsock = lsock;

	rw_init(&V_tcp_tiumapinfo.tmi_maplock, "TCP-in-UDP port map hashtable");
	V_tcp_tiumapinfo.tmi_mapzone = uma_zcreate("tcp-in-udp portmap",
						   sizeof(struct tcp_tiumap),
						   NULL, NULL, NULL, NULL,
						   UMA_ALIGN_PTR, UMA_ZONE_NOFREE);
	 /* FIXME derive set size and hashtable size (below) properly */
	uma_zone_set_max(V_tcp_tiumapinfo.tmi_mapzone, 4096);
	uma_zone_set_warning(V_tcp_tiumapinfo.tmi_mapzone,
			     "TCP-in-UDP portmap full");
	/* XXX Maybe make another malloc type? Doesn't matter that much */
	V_tcp_tiumapinfo.tmi_map = hashinit(256, M_PCB, &V_tcp_tiumapinfo.tmi_hashmask);

	CTR1(KTR_TCPINUDP, "TCP-in-UDP initialized, tunnel on port %d.\n", V_tcp_hijack_port);

	goto out;

 cleanup:
	soclose(lsock);
 out:
	return;
}

/*
 * Tear down TCP-in-UDP state, data structures etc.
 */
static void
tcp_hijack_shutdown(void)
{
	CTR0(KTR_TCPINUDP, "TCP-in-UDP shutdown...\n");

	/* FIXME locking */

	if (V_tcp_hijack_tsock != NULL) {
		soclose(V_tcp_hijack_tsock);
		V_tcp_hijack_tsock = NULL;
	}

	/* FIXME release uma zone etc. */
}

/*
 * Sysctl handler for changing TCP-in-UDP status.
 * Will reinit/shutdown as needed.
 */
static int
tcp_hijack_activate(SYSCTL_HANDLER_ARGS)
{
	int error = 0;
	int new_state = V_tcp_hijack_enabled;

	error = sysctl_handle_int(oidp, &new_state, 0, req);
	CTR3(KTR_TCPINUDP, "tcp_hijack_activate: o=%u, n=%u, e=%d\n",
	     V_tcp_hijack_enabled, new_state, error);

	if (error != 0 || new_state == V_tcp_hijack_enabled) {
		goto out;
	}

	V_tcp_hijack_enabled = new_state;

	/* We don't get here if there wasn't a transition */
	if (!V_tcp_hijack_enabled) {
		tcp_hijack_shutdown();
	} else {
#ifndef TCP_IN_UDP
		panic("TCP-in-UDP enabled without being compiled in! OMGWTFBBQ!");
#endif	/* TCP_IN_UDP */
		tcp_hijack_init();
	}

	

 out:
	return (error);
}

/* Define sysctl subtree for our config */

SYSCTL_NODE(_net_inet_tcp, OID_AUTO, hijack, CTLFLAG_RW, NULL, 
	    "TCP-in-UDP related settings");

SYSCTL_PROC(_net_inet_tcp_hijack, OID_AUTO, enabled,
	    CTLFLAG_RW | CTLFLAG_VNET | CTLTYPE_INT, NULL, 0,
	    tcp_hijack_activate, "UI", "Enable hijacking over UDP");

SYSCTL_UINT(_net_inet_tcp_hijack, OID_AUTO, port, CTLFLAG_RW | CTLFLAG_VNET,
	    &VNET_NAME(tcp_hijack_port), 0, "UDP tunnel port number");

SYSCTL_UINT(_net_inet_tcp_hijack, OID_AUTO, maxretries, CTLFLAG_RW | CTLFLAG_VNET,
	    &VNET_NAME(tcp_tiumaxretries), 0, "Maximum SYN retransmissions over tunnel");

SYSCTL_UINT(_net_inet_tcp_hijack, OID_AUTO, default_disabled, CTLFLAG_RW | CTLFLAG_VNET,
	    &VNET_NAME(tcp_hijack_default_disabled), 0, "Disable TCP-in-UDP by default");



/* Init hook */
VNET_SYSINIT(tcp_hijack, SI_SUB_LAST, SI_ORDER_ANY, tcp_hijack_init, NULL);
