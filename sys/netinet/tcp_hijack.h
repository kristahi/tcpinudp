/*-
 * Copyright (c) 2015 Kristian A. Hiorth <kah@kahnews.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#ifndef _NETINET_TCP_HIJACK_H_
#define _NETINET_TCP_HIJACK_H_

#include <sys/queue.h>
#include <sys/rwlock.h>
#include <sys/mbuf.h>

#include <netinet/in.h>
#include <netinet/in_pcb.h>
#include <netinet/ip.h>
#include <net/route.h>

SYSCTL_DECL(_net_inet_tcp_hijack);

LIST_HEAD(tcp_tiumap_head, tcp_tiumap);

/* Need to access this from several files */
VNET_DECLARE(uint32_t, tcp_hijack_enabled);
#define V_tcp_hijack_enabled VNET(tcp_hijack_enabled)
VNET_DECLARE(struct socket *, tcp_hijack_tsock);
#define V_tcp_hijack_tsock VNET(tcp_hijack_tsock)
VNET_DECLARE(struct tcp_tiumap_info, tcp_tiumapinfo);
#define V_tcp_tiumapinfo VNET(tcp_tiumapinfo)
VNET_DECLARE(uint32_t, tcp_tiumaxretries);
#define V_tcp_tiumaxretries VNET(tcp_tiumaxretries)
VNET_DECLARE(uint32_t, tcp_hijack_port);
#define V_tcp_hijack_port VNET(tcp_hijack_port)
VNET_DECLARE(uint32_t, tcp_hijack_default_disabled);
#define V_tcp_hijack_default_disabled VNET(tcp_hijack_default_disabled)


#define TCPINUDP_MAXFLOWID	0x1f /* 5 bits available */
#define TCPINUDP_MAXFLOWS	(TCPINUDP_MAXFLOWID + 1)
#define TCPINUDP_INVALFLOWID	0xff /* Allocation error */
#define TCPINUDP_SETUPSEGMENT	4    /* Magic offset value for setup seg. */

/* Need this because tcphdr.th_off is a bitfield so we can't use & on it */
#define TCPHDR_OFF(th)		(((uint8_t *)th) + 12)
/* Get the offset field when it's been relocated */
#define TCPHDR_REWRITTENOFF(th) ((*(uint8_t *)th) >> 4)

#ifndef KTR_TCPINUDP
#define KTR_TCPINUDP		0x80000000 /* Last free KTR mask */
#endif

/* TCP-in-UDP control block */
struct tcp_tiucb {
	uint8_t	tiu_flowid;		/* Flow Id */
	int	tiu_setup_attempts;	/* Number of attempted UDP
					 * setups */
};

/* Port mapper */

struct tcp_tiu_conn {
	uint16_t lport;
	uint16_t fport;
	struct tcpcb *tp;
};

struct tcp_tiumap {
	LIST_ENTRY(tcp_tiumap)  tum_mapentry;

	/* What happens with IPv6? */
	/* Addresses are set-once, under mapinfo lock! */
	struct in_addr		tum_faddr;
	struct in_addr		tum_laddr;
	/* Protected by tum_entrylock: */
	struct tcp_tiu_conn	tum_flow[TCPINUDP_MAXFLOWS];
	uint8_t			tum_lastflow;

	struct rwlock		tum_entrylock;
};

#define TCP_TIUMAP_RLOCK(tum)		rw_rlock(&(tum)->tum_entrylock)
#define TCP_TIUMAP_WLOCK(tum)		rw_wlock(&(tum)->tum_entrylock)
#define TCP_TIUMAP_RUNLOCK(tum)		rw_runlock(&(tum)->tum_entrylock)
#define TCP_TIUMAP_WUNLOCK(tum)		rw_wunlock(&(tum)->tum_entrylock)
#define TCP_TIUMAP_UNLOCK(tum)		rw_unlock(&(tum)->tum_entrylock)
#define	TCP_TIUMAP_TRY_UPGRADE(tum)	rw_try_upgrade(&(tum)->tum_entrylock)
#define	TCP_TIUMAP_DOWNGRADE(tum)	rw_downgrade(&(tum)->tum_entrylock)
#define TCP_TIUMAP_LOCK_ASSERT(tum)	rw_assert(&(tum)->tum_entrylock, RA_LOCKED)
#define TCP_TIUMAP_RLOCK_ASSERT(tum)	rw_assert(&(tum)->tum_entrylock, RA_RLOCKED)
#define TCP_TIUMAP_WLOCK_ASSERT(tum)	rw_assert(&(tum)->tum_entrylock, RA_WLOCKED)
#define TCP_TIUMAP_UNLOCK_ASSERT(tum)	rw_assert(&(tum)->tum_entrylock, RA_UNLOCKED)

struct tcp_tiumap_info {
	/* Protected by tmi_maplock: */
	struct tcp_tiumap_head	*tmi_map;
	uma_zone_t		tmi_mapzone;
	u_long			tmi_hashmask;

	struct rwlock		tmi_maplock;
};

#define TCP_TIUMAPHASH(faddr, laddr, mask) 	((faddr ^ laddr) & (mask))

#define TCP_TIUMAPINFO_RLOCK(tmi)		rw_rlock(&(tmi).tmi_maplock)
#define TCP_TIUMAPINFO_WLOCK(tmi)		rw_wlock(&(tmi).tmi_maplock)
#define TCP_TIUMAPINFO_RUNLOCK(tmi)		rw_runlock(&(tmi).tmi_maplock)
#define TCP_TIUMAPINFO_WUNLOCK(tmi)		rw_wunlock(&(tmi).tmi_maplock)
#define TCP_TIUMAPINFO_UNLOCK(tmi)		rw_unlock(&(tmi).tmi_maplock)
#define	TCP_TIUMAPINFO_TRY_UPGRADE(tmi)		rw_try_upgrade(&(tmi).tmi_maplock)
#define	TCP_TIUMAPINFO_DOWNGRADE(tmi)		rw_downgrade(&(tmi).tmi_maplock)
#define TCP_TIUMAPINFO_LOCK_ASSERT(tmi)		rw_assert(&(tmi).tmi_maplock, RA_LOCKED)
#define TCP_TIUMAPINFO_RLOCK_ASSERT(tmi)	rw_assert(&(tmi).tmi_maplock, RA_RLOCKED)
#define TCP_TIUMAPINFO_WLOCK_ASSERT(tmi)	rw_assert(&(tmi).tmi_maplock, RA_WLOCKED)
#define TCP_TIUMAPINFO_UNLOCK_ASSERT(tmi)	rw_assert(&(tmi).tmi_maplock, RA_UNLOCKED)

#define TCP_TIUMTAG		123
#define TCP_TIUMTAG_COOKIE	60560

struct tcp_tiumtag {
	struct m_tag		tiu_tag;
	struct tcp_tiumap	*tiu_map;
};


int tcp_hijack_connect(struct tcpcb *tp);
void tcp_hijack_disconnect(struct tcpcb *tp);
int tcp_hijack_input(struct mbuf **mp, int *offp, int proto);
int tcp_hijack_output(struct mbuf *m, struct mbuf *opt, struct route *ro, int flags,
		      struct ip_moptions *imo, struct inpcb *inp);
int tcp_hijacked(struct tcpcb *tp);
int tcp_hijack_reserveflow(struct in_conninfo *inc, uint8_t flow);
void tcp_hijack_releaseflow(struct in_conninfo *inc, uint8_t flow);
void tcp_hijack_ctlinput(struct ip *ip, int error);

#endif /* _NETINET_TCP_HIJACK_H_ */
